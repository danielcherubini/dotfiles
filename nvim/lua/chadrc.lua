local M = {}

M.ui = {
   theme = "chadracula",
   hl_override = require "configs.highlights",
}

-- M.plugins = "custom.plugins"
-- M.mappings = require "custom.mappings"

return M
